﻿namespace JasonGrimberg_CE08
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbData = new System.Windows.Forms.GroupBox();
            this.lblClassAstric = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.numMonth = new System.Windows.Forms.NumericUpDown();
            this.numCreditHours = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbCourseCode = new System.Windows.Forms.TextBox();
            this.tbClassName = new System.Windows.Forms.TextBox();
            this.rbClassVFW = new System.Windows.Forms.RadioButton();
            this.rbClassASD = new System.Windows.Forms.RadioButton();
            this.tbCourseDesc = new System.Windows.Forms.TextBox();
            this.btnDownloadAndDisplay = new System.Windows.Forms.Button();
            this.gbDescription = new System.Windows.Forms.GroupBox();
            this.gbDownload = new System.Windows.Forms.GroupBox();
            this.lblSelectionAstric = new System.Windows.Forms.Label();
            this.lblDownloadDesc = new System.Windows.Forms.Label();
            this.rdoJSON = new System.Windows.Forms.RadioButton();
            this.rdoXML = new System.Windows.Forms.RadioButton();
            this.statStatusStrip = new System.Windows.Forms.StatusStrip();
            this.tsStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuMainWindow = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSaveMain = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNew = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.pbIcon = new System.Windows.Forms.PictureBox();
            this.gbData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCreditHours)).BeginInit();
            this.gbDescription.SuspendLayout();
            this.gbDownload.SuspendLayout();
            this.statStatusStrip.SuspendLayout();
            this.menuMainWindow.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // gbData
            // 
            this.gbData.Controls.Add(this.lblClassAstric);
            this.gbData.Controls.Add(this.textBox1);
            this.gbData.Controls.Add(this.numMonth);
            this.gbData.Controls.Add(this.numCreditHours);
            this.gbData.Controls.Add(this.label5);
            this.gbData.Controls.Add(this.label4);
            this.gbData.Controls.Add(this.label7);
            this.gbData.Controls.Add(this.label2);
            this.gbData.Controls.Add(this.label1);
            this.gbData.Controls.Add(this.tbCourseCode);
            this.gbData.Controls.Add(this.tbClassName);
            this.gbData.Controls.Add(this.rbClassVFW);
            this.gbData.Controls.Add(this.rbClassASD);
            this.gbData.ForeColor = System.Drawing.Color.White;
            this.gbData.Location = new System.Drawing.Point(12, 129);
            this.gbData.Name = "gbData";
            this.gbData.Size = new System.Drawing.Size(577, 225);
            this.gbData.TabIndex = 2;
            this.gbData.TabStop = false;
            this.gbData.Text = "Course Selection";
            // 
            // lblClassAstric
            // 
            this.lblClassAstric.AutoSize = true;
            this.lblClassAstric.Enabled = false;
            this.lblClassAstric.ForeColor = System.Drawing.Color.DarkRed;
            this.lblClassAstric.Location = new System.Drawing.Point(20, 37);
            this.lblClassAstric.Name = "lblClassAstric";
            this.lblClassAstric.Size = new System.Drawing.Size(15, 20);
            this.lblClassAstric.TabIndex = 10;
            this.lblClassAstric.Text = "*";
            this.lblClassAstric.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(290, 184);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(281, 26);
            this.textBox1.TabIndex = 9;
            this.textBox1.Visible = false;
            // 
            // numMonth
            // 
            this.numMonth.Location = new System.Drawing.Point(173, 153);
            this.numMonth.Name = "numMonth";
            this.numMonth.ReadOnly = true;
            this.numMonth.Size = new System.Drawing.Size(67, 26);
            this.numMonth.TabIndex = 5;
            // 
            // numCreditHours
            // 
            this.numCreditHours.Location = new System.Drawing.Point(173, 188);
            this.numCreditHours.Name = "numCreditHours";
            this.numCreditHours.ReadOnly = true;
            this.numCreditHours.Size = new System.Drawing.Size(67, 26);
            this.numCreditHours.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(52, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 20);
            this.label5.TabIndex = 3;
            this.label5.Text = "Credit Hours: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(96, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "Month: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(56, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 20);
            this.label7.TabIndex = 3;
            this.label7.Text = "Class Name: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(48, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Course Code: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(32, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Class Selection: ";
            // 
            // tbCourseCode
            // 
            this.tbCourseCode.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tbCourseCode.Location = new System.Drawing.Point(173, 118);
            this.tbCourseCode.Name = "tbCourseCode";
            this.tbCourseCode.Size = new System.Drawing.Size(175, 26);
            this.tbCourseCode.TabIndex = 4;
            // 
            // tbClassName
            // 
            this.tbClassName.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tbClassName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbClassName.Location = new System.Drawing.Point(173, 85);
            this.tbClassName.Name = "tbClassName";
            this.tbClassName.Size = new System.Drawing.Size(390, 26);
            this.tbClassName.TabIndex = 3;
            // 
            // rbClassVFW
            // 
            this.rbClassVFW.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbClassVFW.AutoSize = true;
            this.rbClassVFW.ForeColor = System.Drawing.Color.Black;
            this.rbClassVFW.Location = new System.Drawing.Point(262, 33);
            this.rbClassVFW.Name = "rbClassVFW";
            this.rbClassVFW.Size = new System.Drawing.Size(55, 30);
            this.rbClassVFW.TabIndex = 2;
            this.rbClassVFW.TabStop = true;
            this.rbClassVFW.Text = "VFW";
            this.rbClassVFW.UseVisualStyleBackColor = true;
            this.rbClassVFW.CheckedChanged += new System.EventHandler(this.rbClassVFW_CheckedChanged);
            // 
            // rbClassASD
            // 
            this.rbClassASD.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbClassASD.AutoSize = true;
            this.rbClassASD.ForeColor = System.Drawing.Color.Black;
            this.rbClassASD.Location = new System.Drawing.Point(203, 33);
            this.rbClassASD.Name = "rbClassASD";
            this.rbClassASD.Size = new System.Drawing.Size(53, 30);
            this.rbClassASD.TabIndex = 1;
            this.rbClassASD.TabStop = true;
            this.rbClassASD.Text = "ASD";
            this.rbClassASD.UseVisualStyleBackColor = true;
            this.rbClassASD.CheckedChanged += new System.EventHandler(this.rbClassASD_CheckedChanged);
            // 
            // tbCourseDesc
            // 
            this.tbCourseDesc.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.tbCourseDesc.Location = new System.Drawing.Point(20, 25);
            this.tbCourseDesc.Multiline = true;
            this.tbCourseDesc.Name = "tbCourseDesc";
            this.tbCourseDesc.ReadOnly = true;
            this.tbCourseDesc.Size = new System.Drawing.Size(247, 385);
            this.tbCourseDesc.TabIndex = 7;
            // 
            // btnDownloadAndDisplay
            // 
            this.btnDownloadAndDisplay.ForeColor = System.Drawing.Color.Black;
            this.btnDownloadAndDisplay.Location = new System.Drawing.Point(182, 135);
            this.btnDownloadAndDisplay.Name = "btnDownloadAndDisplay";
            this.btnDownloadAndDisplay.Size = new System.Drawing.Size(197, 39);
            this.btnDownloadAndDisplay.TabIndex = 10;
            this.btnDownloadAndDisplay.Text = "Download And Display";
            this.btnDownloadAndDisplay.UseVisualStyleBackColor = true;
            this.btnDownloadAndDisplay.Click += new System.EventHandler(this.btnDownloadAndDisplay_Click);
            // 
            // gbDescription
            // 
            this.gbDescription.Controls.Add(this.tbCourseDesc);
            this.gbDescription.ForeColor = System.Drawing.Color.White;
            this.gbDescription.Location = new System.Drawing.Point(595, 129);
            this.gbDescription.Name = "gbDescription";
            this.gbDescription.Size = new System.Drawing.Size(288, 424);
            this.gbDescription.TabIndex = 3;
            this.gbDescription.TabStop = false;
            this.gbDescription.Text = "Course Description";
            // 
            // gbDownload
            // 
            this.gbDownload.Controls.Add(this.lblSelectionAstric);
            this.gbDownload.Controls.Add(this.lblDownloadDesc);
            this.gbDownload.Controls.Add(this.rdoJSON);
            this.gbDownload.Controls.Add(this.rdoXML);
            this.gbDownload.Controls.Add(this.btnDownloadAndDisplay);
            this.gbDownload.ForeColor = System.Drawing.Color.White;
            this.gbDownload.Location = new System.Drawing.Point(12, 360);
            this.gbDownload.Name = "gbDownload";
            this.gbDownload.Size = new System.Drawing.Size(577, 193);
            this.gbDownload.TabIndex = 9;
            this.gbDownload.TabStop = false;
            this.gbDownload.Text = "Download Data";
            // 
            // lblSelectionAstric
            // 
            this.lblSelectionAstric.AutoSize = true;
            this.lblSelectionAstric.Enabled = false;
            this.lblSelectionAstric.ForeColor = System.Drawing.Color.DarkRed;
            this.lblSelectionAstric.Location = new System.Drawing.Point(181, 94);
            this.lblSelectionAstric.Name = "lblSelectionAstric";
            this.lblSelectionAstric.Size = new System.Drawing.Size(15, 20);
            this.lblSelectionAstric.TabIndex = 10;
            this.lblSelectionAstric.Text = "*";
            this.lblSelectionAstric.Visible = false;
            // 
            // lblDownloadDesc
            // 
            this.lblDownloadDesc.AutoSize = true;
            this.lblDownloadDesc.Location = new System.Drawing.Point(88, 31);
            this.lblDownloadDesc.Name = "lblDownloadDesc";
            this.lblDownloadDesc.Size = new System.Drawing.Size(425, 40);
            this.lblDownloadDesc.TabIndex = 10;
            this.lblDownloadDesc.Text = "Please choose how you would like the data downloaded.\r\nWhich ever is chosen will " +
    "also be how the data is saved out.\r\n";
            // 
            // rdoJSON
            // 
            this.rdoJSON.AutoSize = true;
            this.rdoJSON.Location = new System.Drawing.Point(290, 94);
            this.rdoJSON.Name = "rdoJSON";
            this.rdoJSON.Size = new System.Drawing.Size(76, 24);
            this.rdoJSON.TabIndex = 9;
            this.rdoJSON.TabStop = true;
            this.rdoJSON.Text = "JSON";
            this.rdoJSON.UseVisualStyleBackColor = true;
            this.rdoJSON.CheckedChanged += new System.EventHandler(this.rdoJSON_CheckedChanged);
            // 
            // rdoXML
            // 
            this.rdoXML.AutoSize = true;
            this.rdoXML.Location = new System.Drawing.Point(202, 94);
            this.rdoXML.Name = "rdoXML";
            this.rdoXML.Size = new System.Drawing.Size(67, 24);
            this.rdoXML.TabIndex = 8;
            this.rdoXML.TabStop = true;
            this.rdoXML.Text = "XML";
            this.rdoXML.UseVisualStyleBackColor = true;
            this.rdoXML.CheckedChanged += new System.EventHandler(this.rdoXML_CheckedChanged);
            // 
            // statStatusStrip
            // 
            this.statStatusStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statStatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStatusLabel});
            this.statStatusStrip.Location = new System.Drawing.Point(0, 569);
            this.statStatusStrip.Name = "statStatusStrip";
            this.statStatusStrip.Size = new System.Drawing.Size(894, 22);
            this.statStatusStrip.TabIndex = 10;
            this.statStatusStrip.Text = "Status";
            // 
            // tsStatusLabel
            // 
            this.tsStatusLabel.Name = "tsStatusLabel";
            this.tsStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // menuMainWindow
            // 
            this.menuMainWindow.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuMainWindow.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuMainWindow.Location = new System.Drawing.Point(0, 0);
            this.menuMainWindow.Name = "menuMainWindow";
            this.menuMainWindow.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuMainWindow.Size = new System.Drawing.Size(894, 33);
            this.menuMainWindow.TabIndex = 11;
            this.menuMainWindow.Text = "Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuLoad,
            this.menuSaveMain,
            this.menuNew,
            this.menuExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(50, 29);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // menuLoad
            // 
            this.menuLoad.Name = "menuLoad";
            this.menuLoad.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.menuLoad.Size = new System.Drawing.Size(206, 30);
            this.menuLoad.Text = "&Load...";
            this.menuLoad.Click += new System.EventHandler(this.menuLoad_Click);
            // 
            // menuSaveMain
            // 
            this.menuSaveMain.Name = "menuSaveMain";
            this.menuSaveMain.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menuSaveMain.Size = new System.Drawing.Size(206, 30);
            this.menuSaveMain.Text = "&Save...";
            this.menuSaveMain.Click += new System.EventHandler(this.menuSaveMain_Click);
            // 
            // menuNew
            // 
            this.menuNew.Name = "menuNew";
            this.menuNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menuNew.Size = new System.Drawing.Size(206, 30);
            this.menuNew.Text = "&New";
            this.menuNew.Click += new System.EventHandler(this.menuNew_Click);
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.menuExit.Size = new System.Drawing.Size(206, 30);
            this.menuExit.Text = "E&xit";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // pbIcon
            // 
            this.pbIcon.Image = global::JasonGrimberg_CE08.Properties.Resources.JG;
            this.pbIcon.Location = new System.Drawing.Point(377, 41);
            this.pbIcon.Name = "pbIcon";
            this.pbIcon.Size = new System.Drawing.Size(100, 90);
            this.pbIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbIcon.TabIndex = 12;
            this.pbIcon.TabStop = false;
            this.pbIcon.Click += new System.EventHandler(this.pbIcon_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(894, 591);
            this.Controls.Add(this.pbIcon);
            this.Controls.Add(this.menuMainWindow);
            this.Controls.Add(this.statStatusStrip);
            this.Controls.Add(this.gbDownload);
            this.Controls.Add(this.gbDescription);
            this.Controls.Add(this.gbData);
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "XML JSON API";
            this.gbData.ResumeLayout(false);
            this.gbData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCreditHours)).EndInit();
            this.gbDescription.ResumeLayout(false);
            this.gbDescription.PerformLayout();
            this.gbDownload.ResumeLayout(false);
            this.gbDownload.PerformLayout();
            this.statStatusStrip.ResumeLayout(false);
            this.statStatusStrip.PerformLayout();
            this.menuMainWindow.ResumeLayout(false);
            this.menuMainWindow.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbData;
        private System.Windows.Forms.Label lblClassAstric;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.NumericUpDown numMonth;
        private System.Windows.Forms.NumericUpDown numCreditHours;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbCourseCode;
        private System.Windows.Forms.TextBox tbClassName;
        private System.Windows.Forms.RadioButton rbClassVFW;
        private System.Windows.Forms.RadioButton rbClassASD;
        private System.Windows.Forms.TextBox tbCourseDesc;
        private System.Windows.Forms.Button btnDownloadAndDisplay;
        private System.Windows.Forms.GroupBox gbDescription;
        private System.Windows.Forms.GroupBox gbDownload;
        private System.Windows.Forms.Label lblDownloadDesc;
        private System.Windows.Forms.RadioButton rdoJSON;
        private System.Windows.Forms.RadioButton rdoXML;
        private System.Windows.Forms.StatusStrip statStatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel tsStatusLabel;
        private System.Windows.Forms.MenuStrip menuMainWindow;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuLoad;
        private System.Windows.Forms.ToolStripMenuItem menuSaveMain;
        private System.Windows.Forms.ToolStripMenuItem menuNew;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.Label lblSelectionAstric;
        private System.Windows.Forms.PictureBox pbIcon;
    }
}

