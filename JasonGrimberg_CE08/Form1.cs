﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Xml;
using JasonGrimberg_CE07;

/// <summary>
/// Jason Grimberg
/// CE08 XML and File I/O
/// An application that can download XML or JSON data from a Web address.
/// </summary>
/// 
namespace JasonGrimberg_CE08
{
    public partial class MainWindow : Form
    {
        // Add some instance variables to be used throughout the form
        WebClient apiConnection = new WebClient();

        // API starting point
        string apiStartingPoint = "http://mdv-vfw.com/";

        // API ending point
        string apiEndPoint;

        // Initialization of main window
        public MainWindow()
        {
            InitializeComponent();
        }

        // ---------------------------------------------------------------------
        // Triggers
        // ---------------------------------------------------------------------
        // Download data from API data
        private void btnDownloadAndDisplay_Click(object sender, EventArgs e)
        {
            Download();
        }

        // Menu item to load either JSON or XML file
        private void menuLoad_Click(object sender, EventArgs e)
        {
            LoadFile();
        }

        // Choose the save type and call the specific method
        private void menuSaveMain_Click(object sender, EventArgs e)
        {
            // Create an if statement if the user did not fill out all of the fields
            if (!(tbClassName.Text == "" && tbCourseCode.Text == "" && tbCourseDesc.Text == ""
                && numMonth.Value == 0 && numCreditHours.Value == 0))
            {
                if (rdoJSON.Checked == true)
                {
                    SaveJSONFile();
                }
                else if (rdoXML.Checked == true)
                {
                    SaveXMLFile();
                }
                else
                {
                    // Show Astrix where they can fix their mistake
                    lblSelectionAstric.Visible = true;
                    // Tell the user what is going on
                    MessageBox.Show("Please choose your download method.", "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            // Show message box if all fields are not filled out
            else
            {
                MessageBox.Show("Please download all information before you save.", "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        // Clears the form to the original values
        private void menuNew_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        // Exit the application
        private void menuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // When checked the status will change
        private void rdoXML_CheckedChanged(object sender, EventArgs e)
        {
            lblSelectionAstric.Visible = false;
        }

        // When checked the status will change
        private void rdoJSON_CheckedChanged(object sender, EventArgs e)
        {
            lblSelectionAstric.Visible = false;
        }

        // When checked the status will change
        private void rbClassVFW_CheckedChanged(object sender, EventArgs e)
        {
            lblClassAstric.Visible = false;
        }

        // When checked the status will change
        private void rbClassASD_CheckedChanged(object sender, EventArgs e)
        {
            lblClassAstric.Visible = false;
        }

        // Shows info about the application
        private void pbIcon_Click(object sender, EventArgs e)
        {
            // Pop up of the application info and info within message box
            MessageBox.Show("Created by: Jason Grimberg\r\n" +
                "Coding Exercise 08: XML and File I/O", "App Info");
        }

        // ---------------------------------------------------------------------
        // Methods
        // ---------------------------------------------------------------------
        // Method to download data
        private void Download()
        {
            // Go to method that checks the Internet connection
            bool connectionStatus = CheckConnection();

            // Error handling to make sure a class is selected
            if (rbClassASD.Checked == true || rbClassVFW.Checked == true)
            {
                if (rdoJSON.Checked == true || rdoXML.Checked == true)
                {
                    // Run the build api and read the data
                    if (connectionStatus == true)
                    {
                        BuildClassAPI();
                        if (rdoJSON.Checked == true)
                        {
                            ReadJSONData();
                        }
                        else if (rdoXML.Checked == true)
                        {
                            ReadXMLData();
                        }
                        else
                        {
                            // Show Astrix where they can fix their mistake
                            lblSelectionAstric.Visible = true;
                            // Tell the user what is going on
                            MessageBox.Show("Please choose your download method.",
                                "Oops!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                    }
                    // Throw an error to tell the user what is happening
                    else if (connectionStatus == false)
                    {
                        MessageBox.Show("No Internet connection", "Oops!",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    // Show Astrix where they can fix their mistake
                    lblSelectionAstric.Visible = true;
                    // Tell the user what is going on
                    MessageBox.Show("Please choose your download method.", "Oops!",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            // Tell the user what they did wrong and show where they can fix it
            else
            {
                // Show Astrix where they can fix their mistake
                lblClassAstric.Visible = true;
                // Tell the user what is going on
                MessageBox.Show("Please choose your class.", "Oops!",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        // Method to check if you are connected to the Internet
        private bool CheckConnection()
        {
            // Try to connect to the web
            try
            {
                using (apiConnection = new WebClient())
                {
                    // Try to go to Google for a connection
                    using (apiConnection.OpenRead("http://www.google.com/"))
                    {
                        // If true then this will return true
                        return true;
                    }
                }
            }
            // Catch if this application cannot ping Google
            catch
            {
                return false;
            }
        }

        // Method to read JSON data
        private void ReadJSONData()
        {
            // Download all data as string
            var apiClassData = apiConnection.DownloadString(apiEndPoint);

            // Convert the string to an object
            JObject o = JObject.Parse(apiClassData);

            // Grab the specific data in the object for class title
            string classTitle = o["class"]["course_name_clean"].ToString();

            // Add the title to the form
            tbClassName.Text = classTitle;

            // Grab the specific data in the object for class code
            string classCode = o["class"]["course_code_long"].ToString();

            // Add the course code to the form
            tbCourseCode.Text = classCode;

            // Grab the specific data in the object for course description
            string classDescription = o["class"]["course_description"].ToString();

            // Add vertical scroll bars to the text-box
            tbCourseDesc.ScrollBars = ScrollBars.Vertical;

            // Show the text of the description
            tbCourseDesc.Text = classDescription;

            // Grab the specific data in the object for month
            // Parse out the correct data for the number
            int classMonth = Int32.Parse(o["class"]["sequence"].ToString());

            // Add it to the form
            numMonth.Value = classMonth;

            // Grab the specific data in the object for credit hours
            // Parse out the current data for the number
            int classCreditHours = Int32.Parse(o["class"]["credit"].ToString());

            // Add that data to the form
            numCreditHours.Value = classCreditHours;

        }

        // Method to read XML data
        private void ReadXMLData()
        {
            XmlTextReader apiData = new XmlTextReader(apiEndPoint);

            // Create variable to hold data
            string className = "";
            string courseCode = "";
            string courseDesc = "";
            decimal courseMonth = 0.00m;
            decimal courseCreditHours = 0.00m;

            // loop through the XML data
            while (apiData.Read())
            {
                // When we find the course name
                if(apiData.Name == "course_name_clean")
                {
                    // Get the course name string
                    className = apiData.ReadString();
                }
                // Condition to find the next string
                else if(apiData.Name == "course_code_long")
                {
                    courseCode = apiData.ReadString();
                }
                // Condition to find the next string
                else if(apiData.Name == "course_description")
                {
                    courseDesc = apiData.ReadString();
                }
                // Condition to find the next decimal
                else if(apiData.Name == "sequence")
                {
                    courseMonth = Decimal.Parse(apiData.ReadString());
                }
                // Condition to find the next decimal
                else if(apiData.Name == "credit")
                {
                    courseCreditHours = Decimal.Parse(apiData.ReadString());
                }

                // Add all values to their respected fields
                tbClassName.Text = className;
                tbCourseCode.Text = courseCode;
                // Add scroll bars to the description field
                tbCourseDesc.Text = courseDesc;
                tbCourseDesc.ScrollBars = ScrollBars.Vertical;
                numMonth.Value = courseMonth;
                numCreditHours.Value = courseCreditHours;
            }
        }

        // Method to clear data fields
        private void ClearAll()
        {
            // Clear all of the data fields on the main form
            tbClassName.Clear();
            tbCourseCode.Clear();
            tbCourseDesc.Clear();
            rbClassASD.Checked = false;
            rbClassVFW.Checked = false;
            rdoJSON.Checked = false;
            rdoXML.Checked = false;
            numCreditHours.Value = 0;
            numMonth.Value = 0;
            tsStatusLabel.Text = "";
        }

        // Method to call which radio button is selected
        private string ReturnClassString()
        {
            string classString = "";
            // Check to see which radio button is checked
            // to return the correct string for the API file
            if (rbClassASD.Checked == true)
            {
                classString = "asd";
            }
            else if (rbClassVFW.Checked == true)
            {
                classString = "vfw";
            }

            return classString;
        }

        // Method to build the api string
        private void BuildClassAPI()
        {
            // Grab the class string
            string classString = ReturnClassString();

            // Check to see what data the users wants, JSON or XML
            string jsonOrXml = ".xml";
            if (rdoJSON.Checked)
            {
                jsonOrXml = ".json";
            }

            // Complete the class API string
            apiEndPoint = apiStartingPoint + classString + jsonOrXml;
            
        }

        // Method to load new file
        private void LoadFile()
        {
            // Open dialog new instance
            OpenFileDialog dlg = new OpenFileDialog();

            // Change the title of the open dialog
            dlg.Title = "Open User File";

            // Filter out just text files to open
            dlg.Filter = "TXT files|*.txt|XML files|*.xml";

            // Show the initial directory when the open dialog opens up
            dlg.InitialDirectory = @"C:\";

            // If statement for the opening of the open dialog
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                if(dlg.FilterIndex == 1)
                {
                    string fileName = dlg.FileName;

                    // Create an array of all the lines in the text box
                    string[] filelines = File.ReadAllLines(fileName);

                    // If statement to check first line of code for custom JSON
                    if (filelines[0] != "%")
                    {
                        MessageBox.Show("Not correct file format.", "Error",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (filelines[0] == "%")
                    {
                        // List array of the course save file
                        List<Courses> courseList = new List<Courses>();
                        int linesPerUser = 5;
                        int currUserLine = 0;

                        // Parse line by line into instance of users class
                        Courses courses = new Courses();
                        for (int a = 0; a < filelines.Length; a++)
                        {
                            // Check each line of the file that you are loading
                            if (a != 0 && a % linesPerUser == 0)
                            {
                                courseList.Add(courses);
                                courses = new Courses();
                                currUserLine = 1;
                            }
                            // Move to the next line of the file
                            else
                            {
                                currUserLine++;
                            }

                            // Switch statement to grab each of the lines of code
                            // from text file and populating the form
                            switch (currUserLine)
                            {
                                case 1:
                                    courses.CourseName = filelines[1].Trim();
                                    tbClassName.Text = courses.CourseName;
                                    if (courses.CourseName == "Advanced Scalable Data Infrastructures")
                                    {
                                        rdoJSON.Checked = true;
                                        rbClassASD.Checked = true;
                                    }
                                    else if (courses.CourseName == "Visual Frameworks")
                                    {
                                        rdoJSON.Checked = true;
                                        rbClassVFW.Checked = true;
                                    }
                                    break;
                                case 2:
                                    courses.CourseCode = filelines[2].Trim();
                                    tbCourseCode.Text = courses.CourseCode;
                                    break;
                                case 3:
                                    courses.CourseDescription = filelines[3].Trim();
                                    tbCourseDesc.Text = courses.CourseDescription;
                                    tbCourseDesc.ScrollBars = ScrollBars.Vertical;
                                    break;
                                case 4:
                                    courses.CourseMonth = Decimal.Parse(filelines[4].Trim());
                                    numMonth.Value = courses.CourseMonth;
                                    break;
                                case 5:
                                    courses.CourseCredits = Decimal.Parse(filelines[5].Trim());
                                    numCreditHours.Value = courses.CourseCredits;
                                    MessageBox.Show("Load Complete.", "Loaded", MessageBoxButtons.OK);
                                    break;
                            }
                        }
                    }
                }
                else if(dlg.FilterIndex == 2)
                {
                    // Get the settings for the reader
                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.ConformanceLevel = ConformanceLevel.Document;

                    // Only grab the XML
                    settings.IgnoreComments = true;
                    settings.IgnoreWhitespace = true;

                    // Create the XmlReader
                    using (XmlReader reader = XmlReader.Create(dlg.FileName, settings))
                    {
                        try
                        {
                            // Skip the meta data
                            reader.MoveToContent();

                            // Verify that this is XML data
                            if (reader.Name != "CE08_JasonGrimberg")
                            {
                                // Show the user what happened
                                MessageBox.Show("This is not the correct data format.");
                                return;
                            }
                            else if(reader.Name == "CE08_JasonGrimberg")
                            {
                                rdoXML.Checked = true;

                                // Read the data if this is correct format
                                while (reader.Read())
                                {
                                    // Read only the data we are using
                                    if (reader.Name == "course_name_clean" && reader.IsStartElement())
                                    {
                                        // Read the string into the text box
                                        tbClassName.Text = reader.ReadString();

                                        // Select correct class radio button
                                        if (tbClassName.Text == "Advanced Scalable Data Infrastructures")
                                        {
                                            rbClassASD.Checked = true;
                                        }
                                        else if (tbClassName.Text == "Visual Frameworks")
                                        {
                                            rbClassVFW.Checked = true;
                                        }
                                    }
                                    else if (reader.Name == "course_code_long" && reader.IsStartElement())
                                    {
                                        // Read the string into the text box
                                        tbCourseCode.Text = reader.ReadString();
                                    }
                                    else if (reader.Name == "course_description" && reader.IsStartElement())
                                    {
                                        // Read the string into the text box
                                        tbCourseDesc.Text = reader.ReadString();
                                        tbCourseDesc.ScrollBars = ScrollBars.Vertical;
                                    }
                                    else if (reader.Name == "sequence" && reader.IsStartElement())
                                    {
                                        // Read and parse out the decimal from the string
                                        numMonth.Value = Decimal.Parse(reader.ReadString());
                                    }
                                    else if (reader.Name == "credit" && reader.IsStartElement())
                                    {
                                        // Read and pars out the decimal from the string
                                        numCreditHours.Value = Decimal.Parse(reader.ReadString());
                                    }
                                }

                                // Change the status
                                MessageBox.Show("Load complete");
                            }  
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect format");
                            return;
                        }

                    }
                }
                
            }
        }
        
        // Method to save JSON file to text
        private void SaveJSONFile()
        {
            // Create an if statement if the user did not fill out all of the fields
            if (!(tbClassName.Text == "" && tbCourseCode.Text == "" && tbCourseDesc.Text == ""
                && numMonth.Value == 0 && numCreditHours.Value == 0))
            {
                // Use the SaveFileDialog to open a save dialog box
                using (var sfd = new SaveFileDialog())
                {
                    // Show only text files to write out to
                    sfd.Filter = "Text files (*.txt)|*.txt";

                    // Change the title of the save dialog
                    sfd.Title = "Save JSON to text";

                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        // Write out to text file
                        File.WriteAllText(sfd.FileName, "%\r\n" + tbClassName.Text +
                            "\r\n" + tbCourseCode.Text + "\r\n"
                            + tbCourseDesc.Text + "\r\n" + numMonth.Value + "\r\n" +
                            numCreditHours.Value);

                        // Pop-up showing that the save was successful
                        MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK);
                    }
                }
            }
            // Show message box if all fields are not filled out
            else
            {
                MessageBox.Show("Please fill out all information before you save.", "Oops!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Method to save XML file
        private void SaveXMLFile()
        {
            // Instance of the save dialog
            SaveFileDialog dlg = new SaveFileDialog();

            // Default extension to XML
            dlg.Filter = "XML files (*.xml)|*.xml";

            // Change the title of the save dialog
            dlg.Title = "Save Data to Text";

            // If OK then save will continue
            if (DialogResult.OK == dlg.ShowDialog())
            {
                // Instantiate an XMLWriterSettings object
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;

                // Set the indent to true
                settings.Indent = true;

                // Implement the XmlWriter
                using (XmlWriter writer = XmlWriter.Create(dlg.FileName, settings))
                {
                    // The first element that will define the XML document
                    writer.WriteStartElement("CE08_JasonGrimberg");

                    // Add new element and string that will be a node within a node
                    writer.WriteElementString("course_name_clean", tbClassName.Text);
                    writer.WriteElementString("course_code_long", tbCourseCode.Text);
                    writer.WriteElementString("course_description", tbCourseDesc.Text);
                    writer.WriteElementString("sequence", numMonth.Value.ToString());
                    writer.WriteElementString("credit", numCreditHours.Value.ToString());
                    
                    // Pop-up showing that the save was successful
                    MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK);

                    // Write the end element for the CourseData
                    writer.WriteEndElement();
                }
            }
        }
    }
}
